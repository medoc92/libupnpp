#!/bin/sh

VERSION=`cat LIBUPNPP-VERSION.txt`
VERSION_MAJOR=`echo $VERSION | cut -d . -f 1`
VERSION_MINOR=`echo $VERSION | cut -d . -f 2`
VERSION_REVISION=`echo $VERSION | cut -d . -f 3`

sed -i -E -e '/^#define[ \t]+LIBUPNPP_VERSION/c\'\
"#define LIBUPNPP_VERSION \"$VERSION\"" \
qmk/config.h

sed -i -E -e '/VERSIONCOMMENT/c\'\
"  version: '$VERSION', # VERSIONCOMMENT keep this here, used by setversion.sh" \
meson.build


sed -i -E \
    -e '/^#define[ \t]+LIBUPNPP_VERSION_MAJOR/c\'\
"#define LIBUPNPP_VERSION_MAJOR $VERSION_MAJOR" \
    -e '/^#define[ \t]+LIBUPNPP_VERSION_MINOR/c\'\
"#define LIBUPNPP_VERSION_MINOR $VERSION_MINOR" \
    -e '/^#define[ \t]+LIBUPNPP_VERSION_REVISION/c\'\
"#define LIBUPNPP_VERSION_REVISION $VERSION_REVISION" \
libupnpp/upnpplib.hxx

